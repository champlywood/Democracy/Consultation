<?php
#################
define('VERSION', 1);
if(!is_dir('data')) {mkdir('data');}
$bdd = new PDO('sqlite:data/database.db');
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if(!file_exists('data/config.php')) {
	$bdd->query('CREATE TABLE "proposal" ("id" INTEGER PRIMARY KEY  NOT NULL ,"time" INTEGER, "deadline" INTEGER, "content" TEXT, "open" INTEGER);');
	$bdd->query('CREATE TABLE "debate" ("id" INTEGER PRIMARY KEY  NOT NULL ,"id_proposal" INTEGER, "time" INTEGER, "author" TEXT, "token" TEXT, "status" TEXT, "argument" TEXT);');
	$bdd->query('CREATE TABLE "email" ("id" INTEGER PRIMARY KEY  NOT NULL ,"id_proposal" INTEGER,  "email" TEXT, "poll" INTEGER, "token" TEXT);');
	file_put_contents('data/config.php', '<?php define("ROOT", "http://'.$_SERVER["HTTP_HOST"].'/"); define("QUARANTENEUFTROIS", false); ?>');
}
else {
	include 'data/config.php';
}
include 'app/lib/Parsedown.php';
include 'app/lib/ParsedownExtra.php';

include 'app/core/function.php';
include 'app/core/voting.php';