<?php
function sql($query, $array=array()) {
	global $bdd;
	$retour = $bdd->prepare($query);
	$retour->execute($array);
}
function division($a, $b) {         
	if($b === 0) {
		return null;
	}
	else {
		return $a/$b;
	}
}
function is_april_fool() {
	if(date('d/m', time()) == '01/04' || QUARANTENEUFTROIS == true) {
		return true;
	}
	else {
		return false;
	}
}
if(!function_exists('voting')) {
	function voting($a) {
		if(($a['QUARANTENEUFTROIS'] >= 1) && is_april_fool()) {
			return true;
		}
		elseif(	pourcentage($a['ABSTAIN'],$a['TOTAL']) <= (1/3*100) AND
				pourcentage($a['BLOCK'],$a['TOTAL']) <= 20 AND
				pourcentage($a['WAITING'],$a['TOTAL']) <= (2/5*100) AND
				pourcentage($a['AGREE'],$a['TOTAL']) > 50
			) {
			return true;
		}
		else {
			return false;
		}
	}
}
function timeAgo($time) {
	$timestamp = time()-$time;
	$verbe = ($timestamp > 0) ? 'il y a ' : 'dans ';
	if ($timestamp < 1 AND $timestamp >= 0) {
		return 'maintenant';
	}
	else {
		foreach (array(31104000 => 'an', 2592000 => 'mois', 86400 => 'jour', 3600 => 'heure', 60 => 'minute', 1 => 'seconde') as $secs => $str) {
			if (abs($timestamp/$secs) >= 1) {
				return $verbe.round(abs($timestamp/$secs)).' '.$str.(((round(abs($timestamp/$secs))  >= 2 AND $str !='mois'))?'s':'').'';
			}
	   }
	}
}
function typo($txt) {
	$parse = array(
	// Think http://neamar.fr/Res/Typographe
		// '#&#' => '&amp;',//Esperluette HTML
		'#« #' => '«&nbsp;', // misc (insécable)
		'# »#' => '&nbsp;»',
		'# !#' => '&nbsp;!',
		'# ;#' => '&nbsp;;',
		'# :#' => '&nbsp;:',
		'# \?#' => '&nbsp;?',
		'/\/? &nbsp;\!/' => '&#8253;', //Point exclarrogatif ?!
		'/\/! &nbsp;\?/' => '&#8253;', //Point exclarrogatif !?
		'/\?&nbsp;\!/' => '&#8253;', //Point exclarrogatif ?!
		'/!&nbsp;\?/' => '&#8253;', //Point exclarrogatif !?
		'/\!\?/' =>'&#8253;', //Point exclarrogatif !?
		'/\?\!/' => '&#8253;', //Point exclarrogatif ?!
		'/ °C/' => '&nbsp;&#8451;',		//Degré Celsius
		'/°C/' => '&nbsp;&#8451;',		//Degré Celsius
		'/ °F/' => '&nbsp;&#8457;',		//Degré Fahrenheit
		'/°F/' => '&nbsp;&#8457;',		//Degré Fahrenheit
		'/n°/' => 'n<sup>o</sup>',
		"/'/" => '’',
		"#!{3,}#" => '!!',// !!!!!!!!!!! => !!
		"#\.{4,}#" => '...',// ......... => ...
		'`(\.\.\.)`isU' => '&#8230;',
		"#\s?([\,])\s?#isU" =>'$1 ', //Caractères finaux, de segmentation : . , ...
		"#([0-9]) ,([0-9])#" => '$1,$2',//Nombres écrits avec des virgules.
		"#\. (-?[a-z]\.) #isU" => '.$1',//Et des abréviations ! (penser au - dans le cas de J.-C.)
		"#\. ([-a-z]\.) #sUi" => '.$1',//Et des abréviations ! (à répéter)
		"#([a-z]\.[a-z]\.) ([a-z])#sUi" => '$1$2',//Repérer le dernier si non suivi d'un point
		"#([0-9])(\.|,) ([0-9])#sU" => '$1$2$3',//Et des chiffres !
		'#\n- #isU' => '\n&mdash; ', //Entames de dialogues
		"#([0-9., ][0-9]) ?([%a-z$&]+)(\s|$)#i" => '$1&nbsp;$2$3', // Unités : elles doivent être précédées d'une espace fine insécable.
		"#([0-9])\.([0-9])#i" => '$1,$2', //Nombre décimaux
		"#(\?|\!|\.) \)#" => '$1)' //Parenthèses et ponctuations
	);
	$txt = preg_replace(array_keys($parse),array_values($parse), $txt);
	return $txt;
}
function parse($txt) {
	$md = new ParsedownExtra();
	return typo($md->text($txt));
}
function send_mail($to, $subject, $message, $addheaders=null) {
	 $headers = 'From: webmaster@example.com'.PHP_EOL.'Reply-To: webmaster@example.com'.PHP_EOL.'X-Mailer: PHP/'.phpversion().PHP_EOL.'Content-Type: text/plain; charset="utf-8"; format="fixed"';
	 $headers .= $addheaders;
	 if($_SERVER['REMOTE_ADDR'] =='127.00.00.01' || $_SERVER['REMOTE_ADDR'] == '::1') {error_log(PHP_EOL.'To: '.$to.PHP_EOL.'Object: '.$subject.PHP_EOL.$message, 3, 'data/email.log');} else {mail($to, $subject, $message, $headers);}
}
function base64_url_encode($input) {
 return strtr(base64_encode($input), '+/=', '-_,');
}

function base64_url_decode($input) {
 return base64_decode(strtr($input, '-_,', '+/='));
}
function redirect($url) {
	echo '<script>window.location.replace("'.$url.'");</script>';
}
function pourcentage($part, $total) {
	return round((division($part,$total))*100,2);
}
function infobox($w, $type) {
	echo '<div class="info '.$type.'">'.$w.'</div>';
}
function truncature($content) {
	return strip_tags(parse(substr($content, 0, 100)));
}
function obfuscation_email($email) {
	$part = explode('@',$email);
	$name = substr($part['0'], 0, 3).'***';
	return $name.'@'.$part[1];
}
function avatar($email) {
	return ROOT.'app/lib/cat-avatar/cat-avatar-generator.php?seed='.md5(strtolower(trim($email)));
}
function is_email($email) {
	$email = trim(filter_var($email, FILTER_SANITIZE_EMAIL));
	return filter_var($email, FILTER_VALIDATE_EMAIL);
}
function placeholder_content() {
	global $bdd;
	$participate = array (
		0 =>	array('Maurice Michu', 'maurice@michu.fr'),
		1 =>	array('Gertrude Michu', 'gertrude@michu.fr'),
		2 =>	array('Adam Michu', 'adam@michu.fr'),
		3 => 	array('Bruno Michu ', 'bruno@michu.fr'),
		4 => 	array('Jeanne Michu', 'jeanne@michu.fr'),
		5 => 	array('Camille Michu', 'camille@michu.fr'),
		6 => 	array('Dominique Michu', 'dominique@michu.fr'),
		7 => 	array('Eustache Michu', 'eustache@michu.fr'),
		8 => 	array('Felix Michu', 'felix@michu.fr'),
		9 => 	array('Gaspard Michu', 'gaspard@michu.fr'),
		10 => 	array('Hubert Michu', 'hubert@michu.fr'),
		11 => 	array('Sylvie Dupuis-Morizeau', 'sylvie@dupuis-morizeau.bzh'),
		12 => 	array('Thierry Dupuis-Morizeau', 'Thierry@dupuis-morizeau.bzh'),
		13 => 	array('Kevin Dupuis-Morizeau', 'kevin@dupuis-morizeau.bzh'),
		14 => 	array('Jessica Dupuis-Morizeau', 'Jessica@dupuis-morizeau.bzh')
	);
	$debate = array(
		array('INDIFFERENT', $participate[0][0], 'Je prends pas de sucre dans mon café, donc je suis totalement indifférent !', sha1($participate[0][1])),
		array('ABSTAIN', $participate[1][0], 'Qu’importe le type de sucre, tant qu’il sucre !', sha1($participate[1][1])),
		array('AGREE', $participate[2][0], 'Le sucre de canne, c’est trop bon !', sha1($participate[2][1])),
		array('AGREE', $participate[13][0], '+1', sha1($participate[13][1])),
		array('DISAGREE', $participate[3][0], 'Je pense que la stévia serait meilleur !', sha1($participate[3][1])),
		array('BLOCK', $participate[4][0], 'Oh non, surtout pas du sucre de canne ! C’est importé de l’autre bout du monde, vous avez pensez à l’impact écologique ? Il faut acheter du sucre **local** pour faire vivre l’économie locale !', sha1($participate[4][1])),
		array('QUESTION', $participate[7][0], 'On l’achète dans quel magasin ? N’est-ce pas plus pertinent de l’acheter chez l’épicier du coin ?', sha1($participate[7][1])),
		array('INFORMATION', $participate[4][0], 'Eustache, tu apporte un éclairage pertinent : celui de l’oligopole des supermarchés, imposant leur loi et détruisant le commerce local !', sha1($participate[4][1])),
		array('DISAGREE', $participate[7][0], 'Bon, je refuse de faire vivre le capitalisme ! Achetons chez un commerçant local !', sha1($participate[7][1])),
		array('WAITING', $participate[5][0], 'Je vais attendre de savoir s’il nous reste du sucre, avant d’en racheter !', sha1($participate[5][1])),
		array('AGREE', $participate[6][0], 'OK, pour le sucre de canne ! Pourquoi pas prendre du non raffiné ?', sha1($participate[6][1])),
		array('AGREE', $participate[14][0], 'Oki !', sha1($participate[14][1])),
		array('AGREE', $participate[8][0], ':-)', sha1($participate[8][1])),
		array('NULL', $participate[8][0], 'Sinon, on pourra acheter pour la même occasion du pain ?', sha1($participate[8][1])),
		array('PRO', $participate[12][0], 'En patisserie, c’est meilleur que celui de bettrave !', sha1($participate[12][1])),
		array('ANTI', $participate[11][0], 'Mon amour, c’est mauvais pour ton cholestérol !', sha1($participate[11][1])),
		array('AGREE', $participate[9][0], 'Je peux même me charger d’y aller !', sha1($participate[9][1])),
		array('AGREE', $participate[10][0], 'Excellente initiative !', sha1($participate[10][1])),
	);
	sql('INSERT INTO `proposal`(`time`,`deadline`,`content`, `open`) VALUES (?, ?, ?,?)', array(time(), time()+86400*30, 'Doit-on acheter du *sucre de canne* la prochaine fois qu’on ira au supermarché ?', 0));
	$lastid = $bdd->lastInsertId();
	foreach($participate as $user) {
		$email = is_email($user[1]);
		$token = sha1($email);
		sql('INSERT INTO `email`(`id_proposal`,`email`,`poll`, `token`) VALUES (?, ?, ?, ?)', array($lastid, $email, 0, $token));
	}
	foreach($debate as $d) {
		sql('INSERT INTO `debate`(`id_proposal`,`time`,`author`, `status`, `argument`, `token`) VALUES (?, ?, ?, ?, ?,?)', array($lastid, time(), $d[1], $d[0], $d[2], $d[3]));
		sql('UPDATE `email` SET poll = 1 WHERE token=? AND id_proposal=?', array($d[3], $lastid));
	}
}