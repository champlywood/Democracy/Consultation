<?php
$status_w = array (
	'AGREE' =>'Je suis d’accord avec ça.',
	'ABSTAIN' =>'Je serais content que le groupe décide sans moi.',
	'DISAGREE' =>'Je pense qu’on peut faire mieux.',
	'WAITING' =>'J’attends d’avoir plus d’information avant de me faire un avis.',
	'INDIFFERENT' =>'Je suis indifférent à cette proposition.',
	'BLOCK' =>'J’ai de fortes objections avec cette proposition.',
	'PRO' =>'Apporter un argument en faveur de la proposition.',
	'ANTI' =>'Apporter un argument en défaveur de la proposition.',
	'QUESTION' =>'Apporter une interrogation sur la proposition.',
	'INFORMATION' =>'Apporter un éclairage sur la proposition.',
	'NULL' =>'Apporter une information sans rapport avec le débat.',
);
if(is_april_fool()) { $status_w['QUARANTENEUFTROIS'] =  '49.3. Parce que.';}
$status_type = array(
	'reaction' => array('PRO', 'ANTI', 'QUESTION', 'INFORMATION', 'NULL'),
	'poll' => array('AGREE', 'ABSTAIN', 'DISAGREE', 'WAITING', 'INDIFFERENT', 'BLOCK')
);
if(is_april_fool()) { $status_type['poll'][] = 'QUARANTENEUFTROIS';}
