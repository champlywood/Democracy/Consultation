<blockquote>
	<p><i>Vox populi, vox Dei</i></p>
	<cite>locution latine</cite>
</blockquote>
<h2>Les différentes opinions possibles</h2>
<h3>Les différents types de votes</h3>
<?php foreach($status_type['poll'] as $w) : ?>
	<p><img src="app/assets/<?php echo $w; ?>.png" alt="<?php echo $status_w[$w]; ?>" class="status"/> <?php echo $status_w[$w]; ?></p>
<?php endforeach; ?>
<h3>Les différents types de réactions</h3>
<?php foreach($status_type['reaction'] as $w) : ?>
	<p><img src="app/assets/<?php echo $w; ?>.png" alt="<?php echo $status_w[$w]; ?>" class="status"/> <?php echo $status_w[$w]; ?></p>
<?php endforeach; ?>
<h2>Adoption d’une proposition</h2>
<ul>
	<li><img src="app/assets/INDIFFERENT.png" alt="INDIFFERENT" title="<?php echo $status_w['INDIFFERENT']; ?>"  class="status"/> n’est pas prit en compte dans le calcul final ;</li>
	<li>s’il y a &#8533; (20 %) de <img src="app/assets/BLOCK.png" alt="BLOCK" title="<?php echo $status_w['BLOCK']; ?>" class="status"/>, la proposition est rejetée ; </li>
	<li>s’il y a &#8531; (33,3 %) de <img src="app/assets/ABSTAIN.png" alt="ABSTAIN" title="<?php echo $status_w['ABSTAIN']; ?>" class="status"/>, la proposition est rejetée ;</li>
	<li>s’il y a &#8534; (40 %) de <img src="app/assets/WAITING.png" alt="WAITING" title="<?php echo $status_w['WAITING']; ?>" class="status"/>, la proposition est rejetée ; </li>
	<li>Si <img src="app/assets/AGREE.png" alt="AGREE" class="status" title="<?php echo $status_w['AGREE']; ?>"/> dépasse les 50 %, la proposition est adoptée ;</li>
	<?php if(is_april_fool()) : ?>	<li><img src="app/assets/QUARANTENEUFTROIS.png" alt="AGREE" class="status" title="<?php echo $status_w['QUARANTENEUFTROIS']; ?>"/>, la proposition passe. Parce que. ;</li><?php endif;?>
</ul>