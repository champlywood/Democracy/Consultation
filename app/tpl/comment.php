<div class="comment" id="<?php echo $data['id']; ?>">
	<span class="command">
		<?php echo $delete.$shoutat.$anchor;?>
	</span>
	<img src="app/assets/<?php echo $data['status']; ?>.png" alt="<?php echo $data['status']; ?>" title="<?php echo $status_w[$data['status']]; ?>" class="status"/>
	<?php echo $avatar; ?>
	<?php echo $author; ?> – <time title="<?php echo date('c', $data['time']); ?>"><?php echo timeAgo($data['time']); ?></time>
	<?php echo parse($data['argument']); ?>
</div>
