<form method="post">
	<label for="content">Ma proposition est<textarea id="content" name="content" rows="25" required placeholder="Plus de frites à la cantine !"></textarea></label>
	<label for="deadline">Date-butoir :<input type="datetime" name="deadline" id="deadline" placeholder="YYYY-MM-DD"  value="<?php echo date('Y-m-d', time()+(3600*24*30)); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"/></label>
	<label for="open">Rendre ouvert l’inscription <input type="checkbox" name="open" id="open"/></label>
	<label for="email">
		Liste de courriel des personnes à inviter, séparé par une virgule (sous la forme <code>johndoe@example.org,maurice@example.org</code>…)
		<textarea id="email" name="email" rows="10" required placeholder="johndoe@example.org,maurice@example.org"><?php echo $emails; ?></textarea>
	</label>
	<input type="submit">
	<input type="submit" name="save_emails" value="Sauvegarder les emails">
</form>