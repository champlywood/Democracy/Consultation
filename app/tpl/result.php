<p class="resultpoll">
<?php foreach($status_type['poll'] as $w): ?>
	<span class="result_icon">
		<img src="app/assets/<?php echo $w; ?>.png" alt="<?php echo $status_w[$w]; ?>" class="status" title="<?php echo $status_w[$w]; ?>"/>
		(<?php echo $result[$w];?>) <?php echo ($w !='INDIFFERENT') ? pourcentage($result[$w],$result['TOTAL']).' %' : '';?>
	</span>

<?php endforeach; ?>
</p>
