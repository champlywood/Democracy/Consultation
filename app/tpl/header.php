<!doctype html>
<html lang="fr">
<head>
	<title>Consultation</title>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="app/assets/style.css"/>
	<link rel="stylesheet" href="app/assets/style.old.css"/>
	<script>
		function reply(code) {
			var field = document.getElementsByTagName('textarea')[0];
			field.focus();
			if (field.value !== '') {
				field.value += '\n\n';
			}
			field.value += code;
			field.scrollTop = 10000;
			field.focus();
		}
	</script>
</head>
<body>
<header>
	<h1><a href="index.php">Consultation</a></h1>
</header>
