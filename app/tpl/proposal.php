<article>
	<b><time title=" <?php echo date('c', $data['time']); ?>"> <?php echo timeAgo($data['time']); ?></time></b> et se finissant dans <b><time title="<?php echo date('c', $data['deadline']); ?>"><?php echo timeAgo($data['deadline']); ?></time></b>
	<main><?php echo parse($data['content']); ?></main>
</article>