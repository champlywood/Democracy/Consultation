<?php
define('VERSION', 1);
include 'data/config.php';
error_reporting(-1);
include 'app/inc/i18n/'.LANG.'.php';
#STATUT DES PROPOSITIONS : 0 en cours de validation; 1 validé; 2 rejeté; 3 archivé
include 'app/inc/system.php';
session_start();
include 'app/tpl/header.php';
$action = 'proposal';
if(isset($_GET['action'])){
	switch ($_GET['action']){
		case 'signup': #inscription
			if(!isset($_SESSION['login'])) {
				echo '<h1>'.translate('signup').'</h1>';
				$form = New form();
				$form->startform(array('method'=>'post', 'action'=>'?action=signup'));
				$form->label('email', translate('email'));
				$form->input(array('type'=>'email', 'name'=>'email', 'required'=>'required', 'placeholder'=>'john.doe@example.org'));
				$form->label('password', translate('password'));
				$form->input(array('type'=>'password', 'name'=>'password', 'required'=>'required', 'placeholder'=>'d0_42D!'));
				$form->input(array('type'=>'submit'));
				$form->endform();
				if(isset($_POST['email']) && isset($_POST['password'])) {
					$retour = $bdd->prepare('SELECT email FROM user WHERE email = :email');
					$retour->execute(array('email'=> $_POST['email']));
					$member_exist=$retour->fetch();
					if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
						echo translate('mail_not_valid');
					}
					elseif($member_exist > 0) {
						echo translate('account_exist');
					}
					else {
						$mail = explode("@", $_POST['email']);
						if($mail[1] == DOMAIN) {
							$statut = ADMIN;
						}
						else {
							$statut = USER;
						}
						sql('INSERT INTO user(email, password, statut, pseudo) VALUES(?, ?, ?, ?)', array($_POST['email'],hash('sha512', $_POST['password']), $statut, $mail[0]));
						echo translate('reccorded');
						send_mail($_POST['email'], MAIL, translate('mail_send_pwd_subject'), sprintf(translate('mail_send_pwd_body'), $_POST['password']));
					}
				}
			}
			else {
				redirect('index.php');
			}
		break;
		case 'signin': # connexion
			if(!isset($_SESSION['login'])) {
				echo '<h1>'.translate('signin').'</h1>';
				$form = New form();
				$form->startform(array('method'=>'post', 'action'=>'?action=signin'));
				$form->label('email',  translate('email'));
				$form->input(array('type'=>'email', 'name'=>'email', 'required'=>'required', 'placeholder'=>'john.doe@example.org'));
				$form->label('password',  translate('password'));
				$form->input(array('type'=>'password', 'name'=>'password', 'required'=>'required', 'placeholder'=>'d0_42D!'));
				$form->input(array('type'=>'submit'));
				$form->endform();
				
				if(isset($_POST['email']) && isset($_POST['password'])) {
					$retour = $bdd->prepare("SELECT * FROM user WHERE email =? AND password=?");
					$retour->execute(array($_POST['email'], hash('sha512', $_POST['password'])));
					$donnees = $retour->fetch();
					if ($donnees == true) {
						$_SESSION['login'] = $donnees['email'];
						$_SESSION['user_id'] = $donnees['id'];
						$_SESSION['statut'] = $donnees['statut'];
						$_SESSION['token'] = md5(uniqid(rand(), true));
						redirect('index.php');
					}
				}
			}
			else {
				redirect('index.php');
			}
	break;
	case 'logout':
		if(isset($_SESSION['login'])) {
			$_SESSION = array();
			session_destroy();
			redirect('index.php'); 
		}
		else {
			redirect('index.php'); 
		}
	break;
	case 'profile':
		if(isset($_SESSION['login'])) {
			$retour = $bdd->prepare('SELECT pseudo FROM user WHERE id=?');
			$retour->execute(array($_SESSION['user_id']));
			$data = $retour->fetch();
			$form = New form();
			$form->startform(array('method'=>'post', 'action'=>'?action=profile'));
			$form->label('email',  translate('email'));
			$form->input(array('type'=>'email', 'name'=>'email', 'disabled'=>'disabled', 'value'=>$_SESSION['login']));
			$form->label('password',  translate('password'));
			$form->input(array('type'=>'password', 'name'=>'password'));
			$form->label('password',  translate('pseudo'));
			$form->input(array('type'=>'text', 'name'=>'pseudo', 'value'=>$data['pseudo']));
			$form->input(array('type'=>'submit'));
			$form->endform();
			if(isset($_POST['password']) OR isset($_POST['pseudo'])) {
				if(isset($_POST['password'])) {
					sql('UPDATE user SET password=:password AND pseudo=:pseudo WHERE id=:id', array('password'=>hash('sha512', $_POST['password']), 'pseudo'=>$_POST['pseudo'], 'id'=>$_SESSION['user_id']));
				}
				else {
					sql('UPDATE user SET pseudo=:pseudo WHERE id=:id', array('pseudo'=>$_POST['pseudo'], 'id'=>$_SESSION['user_id']));
				}
				redirect('?action=profile');
			}
		}
	break;
	case 'proposal':
		if(isset($_GET['id_proposal'])) {
			echo '<h1>'.translate('proposal_nav').'</h1>';
			$id = intval($_GET['id_proposal']);
			$retour = $bdd->prepare('SELECT SUM(ballot) AS ballot_sum,
			(SELECT COUNT(*) FROM vote WHERE ballot = -1 AND proposal = v.proposal) AS ballot_neg, 
			(SELECT COUNT(*) FROM vote WHERE ballot = 1 AND proposal = v.proposal) AS ballot_pos,
			(SELECT COUNT(*) FROM vote WHERE ballot = 0 AND proposal = v.proposal) AS ballot_none,
			(SELECT name FROM cat, proposal WHERE p.id = :id) AS categorie,
			(SELECT cat.id FROM cat, proposal WHERE p.id = :id) AS cat_id,
			(SELECT COUNT(*) FROM user) AS nb_user,
			p.id_cat,
			p.id AS id_proposal,
			p.text,
			p.statut
			FROM vote AS v,
			proposal AS p
			WHERE p.id = :id
			AND p.statut = 1');
			$retour->execute(array('id'=>$_GET['id_proposal']));
			$data = $retour->fetch();
			if($_SESSION['login']) {
					$alreadyvote = $bdd->prepare('SELECT COUNT(*) AS count, id_user, proposal, ballot FROM vote WHERE id_user = :id_user AND proposal =:proposal');
					$alreadyvote->execute(array('id_user'=>$_SESSION['user_id'], 'proposal'=>$id));
					$voting = null;
					$alreadyvote_data = $alreadyvote->fetch();
					if($alreadyvote_data['count'] >0) {
						$voted_agree =  ($alreadyvote_data['ballot'] >= 1) ? true : false;
						$voted_not_agree = ($alreadyvote_data['ballot'] <=-1) ? true : false;
						$voted_none =  ($alreadyvote_data['ballot'] == 0) ? true : false;						
					}
					else {
						$voted_agree = false;
						$voted_not_agree = false;
						$voted_none = false;
					}
					$voting .= href('?action=ballot&token='.$_SESSION['token'].'&id_proposal='.$id.'&value_bullet=pos', translate('agree_ballot'), $voted_agree).' (+'.$data['ballot_pos'].') ';
					$voting .= href('?action=ballot&token='.$_SESSION['token'].'&id_proposal='.$id.'&value_bullet=neg', translate('not_agree_ballot'), $voted_not_agree).' (-'.$data['ballot_neg'].') ';
					$voting .= href('?action=ballot&token='.$_SESSION['token'].'&id_proposal='.$id.'&value_bullet=none', translate('none_of_the_above'), $voted_none).' (='.$data['ballot_none'].') ';
			}
			else {
				$voting ='(+'.$data['ballot_pos'].') '.'(-'.$data['ballot_neg'].') '.'(='.$data['ballot_none'].')';
			}
			$abstention = (1-(round(($data['ballot_pos'] + $data['ballot_neg'] + $data['ballot_none'])/$data['nb_user'], 2)))*100;
			include 'app/tpl/viewprop.php';
			##### FORUM ####
			$retour = $bdd->prepare('SELECT DISTINCT f.id AS id_forum, f.time, f.text, u.email, u.pseudo, u.id, (SELECT COUNT(*) FROM vote_forum WHERE ballot = 1 AND id = vote_forum.forum) AS vote FROM forum f, proposal p, user u WHERE f.id_proposal = ? AND u.id = f.id_user AND f.statut=1');
			$retour->execute(array($id));
			while($data= $retour->fetch()) {
				$pseudo =$data['pseudo'];
				$time = date('c', $data['time']);
				$alreadyvote = $bdd->prepare('SELECT (SELECT COUNT(*) FROM vote_forum WHERE ballot = 1) AS count, id_user, forum, ballot FROM vote_forum WHERE forum =:forum');
				$alreadyvote->execute(array('forum'=>$data['id_forum']));
				$alreadyvote_data = $alreadyvote->fetch();
				if(($data['id'] != $_SESSION['user_id'])) {
					$approve = ($alreadyvote_data['ballot']  > 0) ? href('?action=vote_forum&token='.$_SESSION['token'].'&forum_id='.$data['id_forum'].'&approve=no&id_proposal='.$_GET['id_proposal'], translate('not_approve')).' ('.$alreadyvote_data['count'].') ' : href('?action=vote_forum&token='.$_SESSION['token'].'&forum_id='.$data['id_forum'].'&approve=yes&id_proposal='.$_GET['id_proposal'], translate('approve')).' ('.$alreadyvote_data['count'].') ';
				}
				else {
					$approve = '('.$alreadyvote_data['count'].')';
				}
				include ('app/tpl/forum.php');
			}
			if(isset($_SESSION['login'])) {
				$form = New form();
				$form->startform(array('method'=>'post', 'action'=>'?action=proposal&id_proposal='.$id.''));
				$form->textarea(array('name'=>'forum', 'rows'=>'5', 'cols'=>'120'), null);
				$form->input(array('type'=>'submit'));
				$form->endform();
				if(isset($_POST['forum'])) {
					$statut = (MODERATION) ? 0 : 1;
					sql('INSERT INTO forum(id_user, id_proposal, time, text, statut) VALUES (?, ?, ?, ?, ?);', array($_SESSION['user_id'], $id, time(), $_POST['forum'], $statut));
					if(MODERATION) { echo translate('proposal_moderate');}
					redirect('index.php?action=proposal&id_proposal='.$id);
				}
			}
		}
		else {
			echo '<h1>'.translate('proposal_nav').'</h1>';
			if(isset($_GET['order'])) {
				if($_GET['order'] == 'date') {
					$order = 'id';
					$order_date = true;
					$order_vote = false;
				}
				elseif($_GET['order'] == 'vote') {
					$order = '(ballot_pos-ballot_neg)';
					$order_date = false;
					$order_vote = true;
				}
				else {
					$order = '(ballot_pos-ballot_neg)';
					$order_date = false;
					$order_vote = true;
				}
			}
			else {
				$order = '(ballot_pos-ballot_neg)';
				$order_date = false;
				$order_vote = true;
			}
			echo href('?action=proposal&order=date', translate('order_by_date'), $order_date).' '.href('?action=proposal&order=vote', translate('order_by_vote'), $order_vote);
			$retour = $bdd->query('SELECT proposal.id,
			proposal.text,
			proposal.statut,
			(SELECT COUNT(*) FROM vote WHERE ballot = -1 AND proposal=proposal.id) AS ballot_neg,
			(SELECT COUNT(*) FROM vote WHERE ballot = 1 AND proposal=proposal.id) AS ballot_pos,
			(SELECT COUNT(*) FROM vote WHERE ballot = 0 AND proposal=proposal.id) AS ballot_none,
			(SELECT name FROM cat, proposal) AS categorie,
			(SELECT cat.id FROM cat, proposal) AS cat_id,
			(SELECT COUNT(*) FROM user) AS nb_user,
			id AS id_proposal
			FROM proposal
			WHERE proposal.statut = 1
			ORDER BY '.$order.' DESC');
			while($data = $retour->fetch()) {
				$id = $data['id_proposal'];
				if(isset($_SESSION['login'])) {
					$alreadyvote = $bdd->prepare('SELECT COUNT(*) AS count, id_user, proposal, ballot FROM vote WHERE id_user = :id_user AND proposal =:proposal');
					$alreadyvote->execute(array('id_user'=>$_SESSION['user_id'], 'proposal'=>$id));
					$voting = null;
					$alreadyvote_data = $alreadyvote->fetch();
					if($alreadyvote_data['count'] >0) {
						$voted_agree =  ($alreadyvote_data['ballot'] >= 1) ? true : false;
						$voted_not_agree = ($alreadyvote_data['ballot'] <=-1) ? true : false;
						$voted_none =  ($alreadyvote_data['ballot'] == 0) ? true : false;						
					}
					else {
						$voted_agree = false;
						$voted_not_agree = false;
						$voted_none = false;
					}
					$voting .= href('?action=ballot&token='.$_SESSION['token'].'&id_proposal='.$id.'&value_bullet=pos', translate('agree_ballot'), $voted_agree).' (+'.$data['ballot_pos'].') ';
					$voting .= href('?action=ballot&token='.$_SESSION['token'].'&id_proposal='.$id.'&value_bullet=neg', translate('not_agree_ballot'), $voted_not_agree).' (-'.$data['ballot_neg'].') ';
					$voting .= href('?action=ballot&token='.$_SESSION['token'].'&id_proposal='.$id.'&value_bullet=none', translate('none_of_the_above'), $voted_none).' (='.$data['ballot_none'].') ';
				}
				else {
					$voting ='(+'.$data['ballot_pos'].') '.'(-'.$data['ballot_neg'].') '.'(='.$data['ballot_none'].')';
				}
				$abstention = (1-(round(($data['ballot_pos'] + $data['ballot_neg'] + $data['ballot_none'])/$data['nb_user'], 2)))*100;
				include 'app/tpl/viewprop.php';
			}
		}
	break;
	case 'vote_forum':
	if(isset($_SESSION['login']) AND $_GET['token'] == $_SESSION['token'] AND isset($_GET['forum_id'])) {
		$alreadyvote = $bdd->prepare('SELECT COUNT(*) AS count, id_user, forum, ballot FROM vote_forum WHERE id_user = :id_user AND forum =:forum');
		$alreadyvote->execute(array('id_user'=>$_SESSION['user_id'], 'forum'=>$_GET['forum_id']));
		$alreadyvote_data = $alreadyvote->fetch();
		if($_GET['approve'] == 'yes') {
			if($alreadyvote_data['count']  > 0) {
				sql('UPDATE vote_forum SET ballot=? WHERE id_user=? AND forum=?', array("1", $_SESSION['user_id'], $_GET['forum_id']));
				redirect('index.php?action=proposal&id_proposal='.$_GET['id_proposal']);
			}
			else {
				sql('INSERT INTO vote_forum(id_user, ballot, forum) VALUES(?, ?, ?)', array($_SESSION['user_id'], "1", $_GET['forum_id']));
				redirect('index.php?action=proposal&id_proposal='.$_GET['id_proposal']);
			}
		}
		elseif($_GET['approve'] == 'no') {
			sql('UPDATE vote_forum SET ballot=? WHERE id_user=? AND forum=?', array("0", $_SESSION['user_id'], $_GET['forum_id']));
			redirect('index.php?action=proposal&id_proposal='.$_GET['id_proposal']);
		}
		else{}
	}
	break;
	case 'cat':
		echo '<h1>'.translate('cat').'</h1>';
		if(isset($_GET['id_cat'])) {
			$retour = $bdd->prepare('SELECT * FROM cat WHERE id=?');
			$retour->execute(array($_GET['id_cat']));
			$data = $retour->fetch();
			echo '<h1>'.$data['name'].'</h1>';
			echo '<p>'.$data['text'].'</p>';
			$prop = $bdd->prepare('SELECT * FROM proposal WHERE id_cat=?');
			$prop->execute(array($_GET['id_cat']));
			while($data = $prop->fetch()) {
				$id = $data['id'];
				$voting = null;
				$data['cat_id'] =$_GET['id_cat'];
				$data['categorie'] =null;
				include 'app/tpl/viewprop.php';
			}
		}
		else {
			$retour = $bdd->query('SELECT * FROM cat');
			while($data = $retour->fetch()) {
				echo '<h1><a href="?action=cat&id_cat='.$data['id'].'">'.$data['name'].'</a></h1>';
				echo '<p>'.$data['text'].'</p>';
			}
		}
	break;
	case 'addproposal':
		if(isset($_SESSION['login'])) {
			echo '<h1>'.translate('add_proposal').'</h1>';
			$retour = $bdd->query('SELECT COUNT(*) AS count, * FROM cat ORDER BY id DESC');
			$index = 1;
			$cat = array();
			while ($data = $retour->fetch(PDO::FETCH_ASSOC)){
				$cat[$index] = $data['name'];
				$index++;
			}
			$form = New form();
			$form->startform(array('method'=>'post', 'action'=>'?action=addproposal'));
			$form->label('proposal', translate('proposal'));
			$form->textarea(array('name'=>'proposal', 'placeholder'=>'Je voudrais...', 'rows'=>'5', 'cols'=>'120'), null);
			$form->select('cat', $cat, '', true);
			$form->input(array('type'=>'submit'));
			$form->endform();
			
			if(isset($_POST['proposal'])) {
				$statut = (MODERATION) ? "0" : "1";
				sql('INSERT INTO proposal(text, id_cat, statut) VALUES(?, ?, ?)', array($_POST['proposal'], $_POST['cat'], $statut));
				echo translate('proposal_saved');
				if(MODERATION) { echo translate('proposal_moderate');}
				
			}
	}
	else {
		redirect('index.php?action=signup');
	}
	break;
	case 'archive':
		$action = 'archive';
		if(isset($_GET['id_proposal'])) {
			echo '<h1>'.translate('archive').'</h1>';
			$id = intval($_GET['id_proposal']);
			$retour = $bdd->prepare('SELECT SUM(ballot) AS ballot_sum,
			(SELECT COUNT(*) FROM vote WHERE ballot = -1 AND proposal = v.proposal) AS ballot_neg, 
			(SELECT COUNT(*) FROM vote WHERE ballot = 1 AND proposal = v.proposal) AS ballot_pos,
			(SELECT COUNT(*) FROM vote WHERE ballot = 0 AND proposal = v.proposal) AS ballot_none,
			(SELECT name FROM cat, proposal WHERE p.id = :id) AS categorie,
			(SELECT cat.id FROM cat, proposal WHERE p.id = :id) AS cat_id,
			(SELECT COUNT(*) FROM user) AS nb_user,
			p.id_cat,
			p.id AS id_proposal,
			p.text,
			p.statut
			FROM vote AS v,
			proposal AS p
			WHERE p.id = :id
			AND p.statut = 3');
			$retour->execute(array('id'=>$_GET['id_proposal']));
			$data = $retour->fetch();
			$total = $data['ballot_pos'] + $data['ballot_neg'] + $data['ballot_none'];
			$voting ='(+'.(round($data['ballot_pos']/$total, 2)*100).'%) '.'(-'.(round($data['ballot_neg']/$total, 2)*100).'%) '.'(='.(round($data['ballot_none']/$total, 2)*100).'%)';
			$abstention = (1-(round(($data['ballot_pos'] + $data['ballot_neg'] + $data['ballot_none'])/$data['nb_user'], 2)))*100;
			include 'app/tpl/viewprop.php';
			##### FORUM ####
			$retour = $bdd->prepare('SELECT DISTINCT f.id AS id_forum, f.time, f.text, u.email, u.pseudo, u.id, (SELECT COUNT(*) FROM vote_forum WHERE ballot = 1 AND id = vote_forum.forum) AS vote FROM forum f, proposal p, user u WHERE f.id_proposal = ? AND u.id = f.id_user AND f.statut=1');
			$retour->execute(array($id));
			while($data= $retour->fetch()) {
				$pseudo =$data['pseudo'];
				$time = date('c', $data['time']);
				$alreadyvote = $bdd->prepare('SELECT (SELECT COUNT(*) FROM vote_forum WHERE ballot = 1) AS count, id_user, forum, ballot FROM vote_forum WHERE forum =:forum');
				$alreadyvote->execute(array('forum'=>$data['id_forum']));
				$alreadyvote_data = $alreadyvote->fetch();
				$approve = '('.$alreadyvote_data['count'].')';
				include ('app/tpl/forum.php');
			}
		}
		else {
			echo '<h1>'.translate('archive').'</h1>';
			if(isset($_GET['order'])) {
				if($_GET['order'] == 'date') {
					$order = 'id';
					$order_date = true;
					$order_vote = false;
				}
				elseif($_GET['order'] == 'vote') {
					$order = '(ballot_pos-ballot_neg)';
					$order_date = false;
					$order_vote = true;
				}
				else {
					$order = '(ballot_pos-ballot_neg)';
					$order_date = false;
					$order_vote = true;
				}
			}
			else {
				$order = '(ballot_pos-ballot_neg)';
				$order_date = false;
				$order_vote = true;
			}
			echo href('?action=proposal&order=date', translate('order_by_date'), $order_date).' '.href('?action=proposal&order=vote', translate('order_by_vote'), $order_vote);
			$retour = $bdd->query('SELECT proposal.id,
			proposal.text, proposal.statut,
			(SELECT COUNT(*) FROM vote WHERE ballot = -1 AND proposal=proposal.id) AS ballot_neg,
			(SELECT COUNT(*) FROM vote WHERE ballot = 1 AND proposal=proposal.id) AS ballot_pos,
			(SELECT COUNT(*) FROM vote WHERE ballot = 0 AND proposal=proposal.id) AS ballot_none,
			(SELECT name FROM cat, proposal) AS categorie,
			(SELECT cat.id FROM cat, proposal) AS cat_id,
			(SELECT COUNT(*) FROM user) AS nb_user,
			id AS id_proposal
			FROM proposal
			WHERE proposal.statut = 3
			ORDER BY '.$order.' DESC');
			while($data = $retour->fetch()) {
				$id = $data['id_proposal'];
				$total = $data['ballot_pos'] + $data['ballot_neg'] + $data['ballot_none'];
				$voting ='(+'.(round($data['ballot_pos']/$total, 2)*100).'%) '.'(-'.(round($data['ballot_neg']/$total, 2)*100).'%) '.'(='.(round($data['ballot_none']/$total, 2)*100).'%)';
				$abstention = (1-(round(($data['ballot_pos'] + $data['ballot_neg'] + $data['ballot_none'])/$data['nb_user'], 2)))*100;
				include 'app/tpl/viewprop.php';
			}
		}
	break;
	case 'moderation':
		if($_SESSION['statut'] == ADMIN) {
			echo '<h1>'.translate('moderation').'</h1>';
			######PROPOSAL######
			echo '<h2 id="proposal">'.translate('proposal_nav').'</h2>';
			$retour = $bdd->query('SELECT * FROM proposal WHERE proposal.statut = 0 ORDER BY id DESC');
			while($data = $retour->fetch()) {
				$moderation = href('?action=moderation&type=proposal&token='.$_SESSION['token'].'&statut=1&id='.$data['id'], '&#10003;').href('?action=moderation&type=proposal&token='.$_SESSION['token'].'&statut=2&id='.$data['id'], ' &#10060;');
				$id = $data['id'];
				$voting = null;
				$data['cat_id'] = null;
				$data['categorie'] = null;
				include 'app/tpl/viewprop.php';
			}
			######COMMENT######
			echo '<h2 id="comment">'.translate('moderation_comment').'</h2>';
			$retour = $bdd->query('SELECT * FROM forum WHERE forum.statut = 0 ORDER BY id DESC');
			while($data = $retour->fetch()) {
				$moderation = href('?action=moderation&type=forum&token='.$_SESSION['token'].'&statut=1&id='.$data['id'], '&#10003;').href('?action=moderation&type=forum&token='.$_SESSION['token'].'&statut=2&id='.$data['id'], ' &#10060;');
				$pseudo =null;
				$data['id_forum'] = false;
				$time = date('c', $data['time']);
				include 'app/tpl/forum.php';
			}
			######CAT######
			echo '<h2 id="cat">'.translate('cat_add').'';
			if(isset($_GET['type']) && $_GET['type'] == 'edit_cat') {
				$retour = $bdd->prepare('SELECT * FROM cat WHERE id=?');
				$retour->execute(array($_GET['id']));
				$data = $retour->fetch();
				$id = $_GET['id'];
				$name = $data['name'];
				$text = $data['text'];
			}
			else {
				$id = 0;
				$name = '';
				$text = '';
			}
			$form = New form();
			$form->startform(array('method'=>'post', 'action'=>'?action=moderation&type=cat_add'));
			$form->input(array('type'=>'text', 'name'=>'name', 'required'=>'required', 'placeholder'=>'Management', 'value'=>$name));
			$form->textarea(array('name'=>'text', 'rows'=>'5', 'cols'=>'120'), $text);
			$form->input(array('type'=>'hidden', 'name'=>'id', 'value'=>$id));
			$form->input(array('type'=>'submit'));
			$form->endform();
			echo '<h2 id="cat_management">'.translate('cat_management').'</h2>';
			$retour = $bdd->query('SELECT * FROM cat ORDER BY id DESC');
			echo '<table>';
			while($data= $retour->fetch()) {
				echo '<tr>';
				echo '<td>'.$data['name'].'</td>';
				echo '<td>'.$data['text'].'</td>';
				echo '<td>'.href('?action=moderation&type=edit_cat&id='.$data['id'].'#cat', '&#9997;').
							href('?action=moderation&type=cat_management&token='.$_SESSION['token'].'&statut=delete&id='.$data['id'], ' &#10060;').'</td>';
				echo '</tr>';
			}
			echo '</table>';
			echo '<h2 id="user">'.translate('user').'</h2>';
			$retour = $bdd->query('SELECT * FROM user ORDER BY id DESC');
			echo '<table>';
			while($data= $retour->fetch()) {
				$level = ($data['statut'] == 0) ? 	href('?action=moderation&level=up&token='.$_SESSION['token'].'&id='.$data['id'], translate('up_admin')) :
													href('?action=moderation&level=down&token='.$_SESSION['token'].'&id='.$data['id'], translate('down_user'));
													
				echo '<tr>';
				echo '<td>'.$data['email'].'</td>';
				echo '<td>'.$data['pseudo'].'</td>';
				echo '<td>'.$level.'</td>';
				echo '</tr>';
			}
			echo '</table>';
			######PROPOSAL######
			if(isset($_GET['id']) && isset($_GET['token']) && isset($_GET['statut']) && $_GET['token'] == $_SESSION['token'] && $_GET['statut'] == 1 && $_GET['type'] == 'proposal') {
				sql('UPDATE proposal SET statut=1 WHERE id=:id', array('id'=>$_GET['id']));
				redirect('?action=moderation#proposal');
			}
			if(isset($_GET['id']) && isset($_GET['token']) && isset($_GET['statut']) && $_GET['token'] == $_SESSION['token'] && $_GET['statut'] == 2 && $_GET['type'] == 'proposal') {
				sql('UPDATE proposal SET statut=2 WHERE id=:id', array('id'=>$_GET['id']));
				redirect('?action=moderation#proposal');
			}
			if(isset($_GET['id']) && isset($_GET['token']) && isset($_GET['statut']) && $_GET['token'] == $_SESSION['token'] && $_GET['statut'] == 3 && $_GET['type'] == 'proposal') {
				sql('UPDATE proposal SET statut=3 WHERE id=:id', array('id'=>$_GET['id']));
				redirect('?action=archive');
			}
			######COMMENT######
			if(isset($_GET['id']) && isset($_GET['token']) && isset($_GET['statut']) && $_GET['token'] == $_SESSION['token'] && $_GET['statut'] == 1 && $_GET['type'] == 'forum') {
				sql('UPDATE forum SET statut=1 WHERE id=:id', array('id'=>$_GET['id']));
				redirect('?action=moderation#comment');
			}
			if(isset($_GET['id']) && isset($_GET['token']) && isset($_GET['statut']) && $_GET['token'] == $_SESSION['token'] && $_GET['statut'] == 2 && $_GET['type'] == 'forum') {
				sql('UPDATE forum SET statut=2 WHERE id=:id', array('id'=>$_GET['id']));
				redirect('?action=moderation#comment');
			}
			######CAT######
			if(isset($_GET['type']) && $_GET['type'] == 'cat_add' && isset($_POST['name']) && isset($_POST['text']) && isset($_POST['id'])) {
				if($_POST['id'] > 0) {
					sql('UPDATE cat SET name=?, text=? WHERE id=?', array($_POST['name'], $_POST['text'], $_POST['id']));
				}
				else {
					sql('INSERT INTO cat(name, text) VALUES(?, ?)', array($_POST['name'], $_POST['text']));
				}
				redirect('?action=moderation#cat_management');
			}
			if(isset($_GET['id']) && isset($_GET['token']) && isset($_GET['statut']) && $_GET['token'] == $_SESSION['token'] && $_GET['statut'] == 'delete' && $_GET['type'] == 'cat_management') {
				sql('DELETE FROM cat WHERE id=?', array($_GET['id']));
				sql('DELETE FROM proposal WHERE id_cat=?', array($_GET['id']));
				redirect('?action=moderation#cat_management');
			}
			######USER######
			if(isset($_GET['token']) && isset($_GET['level']) && isset($_GET['id'])&& $_GET['token'] == $_SESSION['token'] && $_GET['level'] == 'up') {
				sql('UPDATE user SET statut=1 WHERE id=?', array($_GET['id']));
				redirect('?action=moderation#user');
			}
			if(isset($_GET['token']) && isset($_GET['level']) && isset($_GET['id'])&& $_GET['token'] == $_SESSION['token'] && $_GET['level'] == 'down') {
				sql('UPDATE user SET statut=0 WHERE id=?', array($_GET['id']));
				redirect('?action=moderation#user');
			}
		}
		else {
			echo translate('forbidden_acces');
		}
	break;
	case 'ballot':
		if(isset($_SESSION['login']) && isset($_GET['token']) && $_GET['token'] == $_SESSION['token'] && isset($_GET['value_bullet']) && isset($_GET['id_proposal'])) {
			if($_GET['value_bullet'] == 'pos') { $value_bullet = 1;} elseif($_GET['value_bullet'] == 'neg'){$value_bullet = -1;} elseif($_GET['value_bullet'] == 'none') {$value_bullet = 0;} else {$value_bullet = null;}
			$dejavote = $bdd->prepare('SELECT COUNT(*) AS count, id_user, proposal FROM vote WHERE id_user = :id_user AND proposal =:proposal');
			$dejavote->execute(array('id_user'=>$_SESSION['user_id'], 'proposal'=>$_GET['id_proposal']));
			if($dejavote->fetch()['count'] > 0) {
				sql('UPDATE vote SET ballot=? WHERE id_user=? AND proposal=?', array($value_bullet, $_SESSION['user_id'], $_GET['id_proposal']));
			}
			else {
				sql('INSERT INTO vote(id_user, ballot, proposal) VALUES(?, ?, ?)', array($_SESSION['user_id'], $value_bullet, $_GET['id_proposal']));
			}
			redirect('?action=proposal');
		}
	break;
	}
}
else {
	include 'app/tpl/index.php';
}
include 'app/tpl/footer.php';
?>