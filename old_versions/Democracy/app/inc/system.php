<?php
include 'form.class.php';
if(!file_exists('data/data.sqlite')) {
	mkdir('data/');
	file_put_contents('data/config.php', "<?php ########################################################################
define('MODERATION', false);
define('DOMAIN', 'example.org');
define('MAIL', 'contact@example.org');
define('USER', 0);
define('ADMIN', 1);
define('LANG', 'fr');
error_reporting(0);
########################################################################");
	$bdd = new PDO('sqlite:data/data.sqlite');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$bdd->query('CREATE TABLE "cat" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "name" TEXT, "text" TEXT);');
	$bdd->query('CREATE TABLE "forum" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "id_user" INTEGER, "id_proposal" INTEGER, "time" VARCHAR, "text" TEXT, "statut" INTEGER);');
	$bdd->query('CREATE TABLE "proposal" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "text" TEXT, "statut" INTEGER, "id_cat" INTEGER);');
	$bdd->query('CREATE TABLE "user" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "email" TEXT, "password" TEXT, "statut" INTEGER, "pseudo" TEXT);');
	$bdd->query('CREATE TABLE "vote" ("id" INTEGER PRIMARY KEY  NOT NULL ,"id_user" INTEGER,"ballot" INTEGER DEFAULT (null) ,"proposal" INTEGER DEFAULT (null) );');
	$bdd->query('CREATE TABLE "vote_forum" ("id" INTEGER PRIMARY KEY  NOT NULL ,"id_user" INTEGER,"ballot" INTEGER DEFAULT (null) ,"forum" INTEGER DEFAULT (null) );');
}
else {
	$bdd = new PDO('sqlite:data/data.sqlite');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
function sql($query, $array) {
	global $bdd;
	$retour = $bdd->prepare($query);
	$retour->execute($array);
}
function redirect($url) {
	// echo '<meta http-equiv="refresh" content="0; url='.$url.'">';
	header('Location: '.$url);
	die;
}
function href($href, $label, $button=false) {
	if($button) {
		return $label;
	}
	else {
		return '<a href="'.$href.'">'.$label.'</a>';
	}
}
function translate($array) {
	global $t;
	return $t[$array];
}
function send_mail($to, $from='no-reply@example.org', $subject = '(No subject)', $message = '') {
	$nl = (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $to)) ? $nl = "\r\n" : "\n";
	$message_html = "<html><head></head><body>".$message."</body></html>";
	$message_txt = strip_tags($message);
	$boundary = "-----=".md5(rand());
	$header = "From: ".$from."".$nl;
	$header.="X-Mailer: PHP/".phpversion();
	$header.= "MIME-Version: 1.0".$nl;
	$header.= "Content-Type: multipart/alternative;".$nl." boundary=\"$boundary\"".$nl;
	$body = $nl."--".$boundary.$nl;
	$body.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$nl;
	$body.= "Content-Transfer-Encoding: 8bit".$nl;
	$body.= $nl.$message_txt.$nl;
	$body.= $nl."--".$boundary.$nl;
	$body.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$nl;
	$body.= "Content-Transfer-Encoding: 8bit".$nl;
	$body.= $nl.$message_html.$nl;
	$body.= $nl."--".$boundary."--".$nl;
	$body.= $nl."--".$boundary."--".$nl;
	mail($to,'=?UTF-8?B?'.base64_encode($subject).'?=',$body,$header);
}
?>