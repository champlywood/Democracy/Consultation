<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<link href='http://fonts.googleapis.com/css?family=Tangerine' rel='stylesheet' type='text/css'>
<link rel="stylesheet"  type="text/css" href="app/tpl/design.css">
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<title>Democracy 2.0</title>
</head>
<body>
<header>
	<h1><a href="?">Democracy 2.0</a></h1>
	<div class="connexion">
	<?php
		if(!isset($_SESSION['login'])) {
			echo '<span class="button-signup">'.href('?action=signup', translate('join')).'</span>';
			$form = New form();
			$form->startform(array('method'=>'post', 'action'=>'?action=signin'));
			$form->input(array('type'=>'email', 'name'=>'email', 'required'=>'required', 'placeholder'=>'john.doe@example.org'));
			$form->input(array('type'=>'password', 'name'=>'password', 'required'=>'required', 'placeholder'=>'d0_42D!'));
			$form->input(array('type'=>'submit'));
			$form->endform();
		}
		else {
			echo '<div class="box-connected">';
			if(isset($_SESSION['login'])) {echo href('?action=profile', '<b>'.$_SESSION['login'].'</b>');}
			if(isset($_SESSION['login']) && $_SESSION['statut'] == 1) {echo href('?action=moderation', translate('moderation'));}
			if(isset($_SESSION['login'])) {echo href('?action=logout', translate('logout'));}
			echo '</div>';
		}
	?>
</div>
	<nav>
<?php echo href('?action=proposal',translate('proposal_nav')); ?>
<?php echo href('?action=cat',translate('cat')); ?>
<?php echo href('?action=archive',translate('archive')); ?>
<?php echo href('?action=addproposal', translate('add_proposal')); ?>
<?php if(!isset($_SESSION['login'])) {echo href('?action=signup', translate('signup'));} ?>
<?php if(!isset($_SESSION['login'])) {echo href('?action=signin',translate('signin'));} ?>
</nav>
</header>

<main>